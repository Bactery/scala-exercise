package currencies.services

import java.time.LocalDate

import currencies.models._


class CurrencyRateService(currencyRates: Seq[CurrencyRatesOnDate]) {

  /**
    * List all currency rates on each day, for each currency.
    */
  def list(): Seq[CurrencyRatesOnDate] = {
   currencyRates.map(currencyRate => currencyRate)
  }

  /**
    * List all currency rates on the specified day, for each currency.
    */
  def listRatesForDate(date: LocalDate): Option[CurrencyRatesOnDate] = {
    currencyRates.find((currencyRate: CurrencyRatesOnDate) => date == currencyRate.date)
  }

  /**
    * List the rate for the specified currency, on the specified day.
    */
  def getRateForCurrencyAndDate(date: LocalDate, currency: Currency): Option[CurrencyRate] = {
    listRatesForDate(date).map(currencyRate => currencyRate.rates.find(rate => rate.currency == currency)).getOrElse(None)
  }

  /**
    * List the rate of the specified currency on each of the available day.
    */
  def getCurrencyHistory(currency: Currency): Map[LocalDate, BigDecimal] = {
    (for (cR <- currencyRates; r <- cR.rates if r.currency == currency) yield cR.date -> r.rate).toMap
  }

  /**
    * For each couple of days (day1, day2), return whether the currency rate went up or down from day1 to day2.
    *
    * Note: since the webservice returns the days in reverse chronological order, the trend will also be reversed!
    */
  def getCurrencyTrend(currency: Currency): Seq[Direction] = {
    val trend = for (cR <- currencyRates; r <- cR.rates if r.currency == currency) yield r.rate
    val trendPairs = trend.sliding(2).toList
    trendPairs.map(rate => {
      rate match {
        case x if rate(0) > rate(1) => Direction.Up
        case x if rate(0) < rate(1) => Direction.Down
        case x if rate(0) == rate(1) => Direction.Stable
      }
    }).reverse
  }
}
