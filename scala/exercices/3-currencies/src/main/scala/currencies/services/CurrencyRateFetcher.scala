package currencies.services

import java.time.LocalDate

import scala.concurrent._
import ExecutionContext.Implicits.global
import play.api.libs.ws.ahc.StandaloneAhcWSClient
import currencies.models.{Currency, CurrencyRate, CurrencyRatesOnDate}


object CurrencyRateFetcher {

  private val wsClient = StandaloneAhcWSClient() // Documentation: https://www.playframework.com/documentation/2.6.x/ScalaWS

  /**
    * Fetch the available currency rates over HTTP.
    *
    * The method returns a Left containing the network error message, if any. Otherwise, it returns a Right containing
    * the currency rates.
    */
  def fetch(url: String):  Future[Either[String, Seq[CurrencyRatesOnDate]]] = {
    wsClient.url(url).get().map { response =>
      if (response.status == 200) {
        val body: scala.xml.Elem = scala.xml.XML.loadString(response.body)
        val result = for (cRoD <- (body \ "Cube"); cR <- (cRoD \ "Cube")) yield CurrencyRatesOnDate(LocalDate.parse((cR \ "@time").text), (for (c <- (cR \ "Cube")) yield CurrencyRate(Currency((c \ "@currency").text), BigDecimal((c \ "@rate").text))))
        Right(result.toSeq)
      } else {
        Left(s"Server responded with status : ${response.status}")
      }
    }
  }

}
